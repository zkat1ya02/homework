import React from "react"; 
import StarIcon from "../../icons/star";
import { Link } from "react-router-dom";
import Cart from "../../icons/cart";
import './SideBar.scss';


const SideBar = () =>(
    <nav >
      <ul className = 'sidebar'>
        <li>
          <Link to = "/items">Items</Link>
        </li>
      <div className = 'add-section'>
       <li>
          <Link to = "/cart"><Cart/>Cart</Link>
      </li>
      <li>
        <Link to = "/favorites"><StarIcon filled/>Favorites</Link>
      </li>
      </div>
      </ul>
    </nav>
)

export default SideBar;