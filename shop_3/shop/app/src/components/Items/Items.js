import React, {useEffect} from "react";
import Item from "../Item/Item";
import Modal from "../Modal/Modal";
import './Items.scss';

const Items = ({items, favoritesArray,  cartArray,  setCart, setFavorites, modalToCart, setModalToCart, selectedItemToCart, setselectedItemToCart, openModal,
    closeModal, setItemToCart, btnAddtoCart,  btnRemoveItemFromCart, addToCartModal, removeItemFromCart}) => {

        useEffect(() => {
            setCart(cartArray)
            }, [cartArray, setCart]);

    const productItems = items.map(item => 
    <Item 
    key = {item.id}
    openModal = {openModal} 
    closeModal = {closeModal} 
    item = {item} 
    btnAddtoCart = {btnAddtoCart}
    cartArray = {cartArray}
    favoritesArray = {favoritesArray} 
    setFavorites = {setFavorites} 
    btnRemoveItemFromCart = {btnRemoveItemFromCart}
    />
    )
    return(
        <>
            <ol className="products__wrapper">{productItems}</ol>
            {modalToCart&&
            <Modal
             modalToCart={modalToCart} 
             closeModal={closeModal}
             openModal={openModal} 
             selectedItemToCart={selectedItemToCart}   
             setItemToCart = {setItemToCart}
             addToCartModal = {addToCartModal}
             removeItemFromCart = {removeItemFromCart}
              />
              }
        </>
        
    )
}

export default Items;

