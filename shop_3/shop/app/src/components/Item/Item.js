import React, {useState, useEffect} from "react";
import './item.scss';
import StarIcon from '../../icons/star.jsx';

const Item = ({item, openModal, closeModal, favoritesArray, cartArray, setFavorites, btnAddtoCart,  btnRemoveItemFromCart}) =>{
    const [filledStar, setFilledStar] = useState(false);
    const {title, price, url, id, color} = item;

    useEffect(() => {
        try{
            favoritesArray = []
            cartArray = []
            cartArray.push(...JSON.parse(localStorage.getItem('favorites')))
            favoritesArray.push(...JSON.parse(localStorage.getItem('favorites')))
            favoritesArray.forEach((item)=>{   
                if(item.id.includes(id)){
                    setFilledStar(true);
                }
            })
        }catch(e){
            console.log(e);
        }    
    }, [favoritesArray, cartArray, id])

    const addToFavorite = (item) =>{
        favoritesArray.push(item);
        setFilledStar(true);
        setItemToFavorites();
      }
    
      const removeFavorite = (item) =>{
          let favorites = JSON.parse(localStorage.getItem('favorites'));
          let filteredFavorite = favorites.filter((element) =>{
              return element.id !== item.id;
          })
        localStorage.setItem('favorites', JSON.stringify(filteredFavorite));
        setFilledStar(false);
        favoritesArray = [];
        let arrFromObj = filteredFavorite.map(favorite => {return favorite});
        favoritesArray.push(...arrFromObj);
        setFavorites(favoritesArray);
      }

      const setItemToFavorites = () =>{
        let favorites = JSON.parse(localStorage.getItem('favorites'));
        if (!favorites){
            favorites = [];
        } 
        const selectedItem ={
          title: title,
          id: id,
          price: price,
          url: url,
          color: color,
        }
        favorites.push(selectedItem);
        localStorage.setItem('favorites', JSON.stringify(favorites));
        closeModal();
      }
    return(
        <div className = "card">
        <img className = "productItem__img" alt="product" src = {url}/> 
         <h1 className = "productItem__name"> 
         {title} 
         {filledStar && <div onClick={() => removeFavorite(item)}> {StarIcon(filledStar)} </div> }
         {!filledStar && <div onClick={() => addToFavorite(item)}> {StarIcon(filledStar) } </div> }
         </h1>
        <p className = "price">
            {price} USD
        </p>
        <p>
            {color}
        </p>
        {btnAddtoCart&& <p> <button onClick={() => openModal(item)}>Add to Cart</button></p>}
        {btnRemoveItemFromCart&& <p> <button onClick={() => openModal(item)}>Remove from Cart</button> </p>}
        </div>
   
    )
}

export default Item;