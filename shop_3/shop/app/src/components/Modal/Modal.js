import React from "react"
import './Modal.scss'

const Modal = ({closeModal, selectedItemToCart, setItemToCart, addToCartModal, removeItemFromCartModal, removeItemFromCart}) =>(

  <div  className = 'modal__wrapper ' onClick = {closeModal}>
      <div className = 'modal__body' onClick = {e => e.stopPropagation()} >
        <div className = 'modal__close' onClick = {closeModal}> x </div>
        {addToCartModal&&
        <>
        <h2>{`Do you want to add ${selectedItemToCart.title} to shopping cart?`}</h2>
        <hr />
       <button className = "btn" onClick = {setItemToCart}> yes </button>
       <button className = "btn" onClick = {closeModal}> no </button>
       </>
       }
       {removeItemFromCartModal&&
       <>
       <h2>{`Do you want to remove ${selectedItemToCart.title} from the shopping cart?`}</h2>
        <button className = "btn" onClick = {removeItemFromCart}> yes </button>
       <button className = "btn" onClick = {closeModal}> no </button>
       </>
       }
      </div>
  </div>
)


export default Modal;