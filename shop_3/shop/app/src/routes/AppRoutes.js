import React, { useEffect } from "react";
import { Route } from "react-router";
import { Switch, Redirect} from "react-router-dom";
import Items from "../components/Items/Items";
import Cart from "../pages/Cart/Cart";
import Favorites from "../pages/Favorites/Favorites";
import Page404 from "../pages/Page404/Page404";

const AppRoutes = ({items, favoritesArray, cartArray, setCart ,setFavorites, modalToCart, setModalToCart, selectedItemToCart, setselectedItemToCart, openModal,
  closeModal, setItemToCart, btnAddtoCart, btnRemoveItemFromCart, setItems, addToCartModal, removeItemFromCartModal, removeItemFromCart}) =>{

    useEffect(() => {
    setCart(cartArray)
    }, [cartArray, setCart]);

    return(
        <Switch>
        <Redirect exact from = '/' to = '/items'/>
          <Route exact path = "/items">
        <Items 
        items = {items}
        setCart = {setCart}
        cartArray = {cartArray}
        removeItemFromCart = {removeItemFromCart}
        addToCartModal = {addToCartModal}
        favoritesArray = {favoritesArray} 
        setFavorites = {setFavorites}
        openModal = {openModal}
        btnAddtoCart = {btnAddtoCart}
        closeModal = {closeModal}
        modalToCart= {modalToCart} 
        setItemToCart = {setItemToCart}
        selectedItemToCart = {selectedItemToCart} 
        />
          </Route>
          <Route exact path = "/cart">
          <Cart
            items = {items}
            setCart = {setCart}
            cartArray = {cartArray}
            removeItemFromCart = {removeItemFromCart}
            removeItemFromCartModal = {removeItemFromCartModal}
            favoritesArray = {favoritesArray}
            setFavorites = {setFavorites}
            openModal = {openModal}
            closeModal = {closeModal}
            modalToCart = {modalToCart}
            // setItems = {setItems}
            setItemToCart = {setItemToCart}
            selectedItemToCart = {selectedItemToCart} 
            btnRemoveItemFromCart = {btnRemoveItemFromCart}
          />
          </Route>
          <Route  exact path = "/favorites">
          <Favorites
            setCart = {setCart}
            cartArray = {cartArray}
             modalToCart = {modalToCart}
             addToCartModal = {addToCartModal}
             openModal = {openModal}
             closeModal = {closeModal}
             btnAddtoCart = {btnAddtoCart}
             setItemToCart = {setItemToCart}
             selectedItemToCart = {selectedItemToCart} 
          />
          </Route>
          <Route path='*'>
            <Page404/>
          </Route>
          </Switch>
    )
};

export default AppRoutes;