import React, {useEffect, useState} from 'react';
import axios from 'axios';
import SideBar from './components/SideBar/SideBar';
import AppRoutes from './routes/AppRoutes';

const App = () => {
  const [items, setItems] = useState([]);
  const [favoritesArray, setFavorites] = useState(JSON.parse(localStorage.getItem('favorites')) || []);
  const [cartArray, setCart] = useState(JSON.parse(localStorage.getItem('cart')) || []);
  const [addToCartModal] = useState(true);
  const [removeItemFromCartModal] = useState(true);
  const [modalToCart, setModalToCart] = useState(false);
  const [btnAddtoCart, ] = useState(true);
  const [btnRemoveItemFromCart,] = useState(true);
  const [selectedItemToCart, setselectedItemToCart] = useState(null);

  
  useEffect(() => {
    axios('/api/items')
      .then(res => {
        setItems(res.data);
      })
  }, []);

  const openModal = (item) =>{
    console.log(item);
    setModalToCart(true);
    setselectedItemToCart(item);
  }

  const closeModal = () =>{
    setModalToCart(false);
  }

  const setItemToCart = () =>{
    let cart = JSON.parse(localStorage.getItem('cart')) || [];
    cart.push(selectedItemToCart);
    cartArray.push(selectedItemToCart);
    setCart(cartArray);
    localStorage.setItem('cart', JSON.stringify(cart));
    closeModal();
    alert(`You added ${selectedItemToCart.title} to shopping cart`);
  }

  const removeItemFromCart = () =>{
    let cart = JSON.parse(localStorage.getItem('cart'));
    let filteredCart = cart.filter((element) =>{
          return element.id !== selectedItemToCart.id;
      })
      cart = [];
      let arrFromObj = filteredCart.map(cart => {return cart});
      cart.push(...arrFromObj);
      localStorage.setItem('cart', JSON.stringify( cart));
      setCart(cart);
      closeModal();
      alert(`You removed ${selectedItemToCart.title} from the shopping cart`);
  }

  return (
    <div className = "App">
      <SideBar/>
      <AppRoutes
       addToCartModal = {addToCartModal}
       removeItemFromCart = {removeItemFromCart}
       removeItemFromCartModal = {removeItemFromCartModal}
       items = {items} 
       favoritesArray = {favoritesArray} 
       cartArray = {cartArray}
       setCart = {setCart}
       setFavorites = { setFavorites} 
       openModal = {openModal}
       closeModal = {closeModal}
       modalToCart = {modalToCart} 
       btnAddtoCart = {btnAddtoCart}
       btnRemoveItemFromCart = { btnRemoveItemFromCart}
       setModalToCart = {setModalToCart} 
       setItemToCart = {setItemToCart}
       setItems = {setItems}
       selectedItemToCart = {selectedItemToCart} 
       setselectedItemToCart = {setselectedItemToCart}
      />
    </div>
  )
}

export default App;
