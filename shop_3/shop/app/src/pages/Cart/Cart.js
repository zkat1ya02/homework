import React from "react"
import {useState, useEffect } from "react";
import axios from 'axios';
import Item from "../../components/Item/Item";
import Modal from "../../components/Modal/Modal";


const Cart = ({selectedItemToCart, items, setCart, cartArray, favoritesArray, setFavorites,  openModal, closeModal, modalToCart, setItemToCart, btnRemoveItemFromCart,  removeItemFromCartModal, removeItemFromCart }) =>{
    const [unique, setUniqueItems] = useState([]);
    useEffect(() => {
    try{
        if (items.length === 0){
            axios('/api/items')
            .then(res => {
                items.push(...res.data);
            let uniqueProductItems = [];
            if (cartArray){
                const uniqueArray = [...new Set(cartArray.map(item => item.id))];
                for (let i=0; i<uniqueArray.length; i++){
                    let result = items.find(obj =>{
                        return obj.id === uniqueArray[i].toString()
                    })
                    uniqueProductItems.push(result)
                }
                setUniqueItems(uniqueProductItems)
            }
          
        
            })
        } else{
                  let uniqueProductItems = [];
            const uniqueArray = [...new Set(cartArray.map(item => item.id))];
              for (let i=0; i<uniqueArray.length; i++){
                  let result = items.find(obj =>{
                      return obj.id === uniqueArray[i].toString()
                  })
                  uniqueProductItems.push(result)
              }
              setUniqueItems(uniqueProductItems)
        
        }      
    }catch(e){
        console.log(e);
    }
        
    }, [cartArray, items])

    const productItems = () =>{ 

        if (cartArray.length > 0)
        {
             const productItems = unique.map(item => 
        <Item 
        key={item.id}
        favoritesArray ={favoritesArray}
        setFavorites = {setFavorites}
        setCart = {setCart}
        openModal={openModal} 
        closeModal={closeModal} 
        item={item}   
        btnRemoveItemFromCart = {btnRemoveItemFromCart}
        />)
                return productItems
        }
        return <ol>There is no prodact in the cart</ol>
     
    } 
    
 return(
    <>
     {!cartArray&&<ol className = "products__wrapper">The cart is empty</ol>}
     {cartArray&&<ol className = "products__wrapper">{productItems()}</ol>}
     {modalToCart&&
     <Modal
     modalToCart={modalToCart} 
     closeModal={closeModal} 
     openModal={openModal} 
     selectedItemToCart={selectedItemToCart} 
     setItemToCart = {setItemToCart}
     removeItemFromCartModal = {removeItemFromCartModal}
     removeItemFromCart = {removeItemFromCart}
     />
     } 
    </>
    )
}

export default Cart;

