import React from "react"
import './Page404.scss';

const Page404 = () =>{

 return(
    <div className='page__404'>
        <div>
          <h1>Awww... Don't cry(</h1> 
          <h4> It's just a 404 Error</h4> 
        </div>
    </div>
    )
}

export default Page404;