import React, {useState} from "react"
import Item from "../../components/Item/Item";
import Modal from "../../components/Modal/Modal";


const Favorites = ({openModal, closeModal, setCart, modalToCart, setItemToCart, selectedItemToCart, btnAddtoCart, addToCartModal }) =>{
    const [favoritesArray, setFavorites] = useState(JSON.parse(localStorage.getItem('favorites')));

    const productItems = () =>{
        if (favoritesArray.length > 0){
            const listOfSelectedItems = favoritesArray.map(item => 
                <Item 
                key = {item.id}
                openModal = {openModal} 
                closeModal = {closeModal} 
                setCart = {setCart}
                item = {item} 
                btnAddtoCart = {btnAddtoCart}
                favoritesArray = {favoritesArray} 
                setFavorites = {setFavorites} 
              
                />
                )
                return listOfSelectedItems
        }
        return <ol>There is no prodact in selected</ol>
     
    } 

 return(
    <>
     {!favoritesArray && <ol className="products__wrapper">There is no prodact in selected</ol>}
     {favoritesArray && <ol className="products__wrapper">{productItems()}</ol>}
     {modalToCart && 
     <Modal 
     modalToCart={modalToCart} 
     closeModal={closeModal} 
     openModal={openModal} 
     selectedItemToCart={selectedItemToCart} 
     setItemToCart = {setItemToCart}
     addToCartModal = {addToCartModal}
     />
     } 
    </>
    )
}

export default Favorites;
