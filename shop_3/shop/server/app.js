const express = require('express');

const app = express();

const port = 8085;

const items = [
  { "title": "asian cat", "price":"300", "url":"https://ireland.apollo.olxcdn.com/v1/files/ydkft4lyuqno1-UA/image;s=644x461", "id":"34534590", "color": "brown"},
  { "title": "devon rex", "price": "400", "url": "https://ireland.apollo.olxcdn.com/v1/files/ccbkzlyuhg73-UA/image;s=644x461", "id": "34532390", "color": "brown"},
  {"title": "cymric cat", "price": "120", "url": "https://ireland.apollo.olxcdn.com/v1/files/bu36ehy6459v-UA/image;s=644x461", "id": "34534545", "color": "brown"},
  { "title": "nebelung", "price": "500", "url": "https://ireland.apollo.olxcdn.com/v1/files/0dav3dt9inml2-UA/image;s=644x461", "id": "34532391", "color": "brown"},
  { "title": "peterbald", "price": "150", "url": "https://ireland.apollo.olxcdn.com/v1/files/phkk47rpthet-UA/image;s=644x461", "id": "34879545", "color": "brown"},
  {"title": "birman", "price": "370", "url": "https://ireland.apollo.olxcdn.com/v1/files/6e6t0ie65w27-UA/image;s=644x461", "id":"14879545", "color": "brown"},
  { "title": "sokoke", "price": "200", "url": "https://ireland.apollo.olxcdn.com/v1/files/ck0guq157pex1-UA/image;s=644x461", "id": "14579545", "color": "black"},
  { "title": "chartreux", "price": "320", "url": "https://ireland.apollo.olxcdn.com/v1/files/1yals3loxs4l-UA/image;s=644x461", "id": "14578545", "color": "white"},
  { "title": "toyger", "price": "180", "url": "https://ireland.apollo.olxcdn.com/v1/files/h9mlsixga5jc-UA/image;s=644x461", "id": "14578546", "color": "brown"},
  { "title": "ragdoll", "price": "400", "url": "https://ireland.apollo.olxcdn.com/v1/files/36c5enrqqivr1-UA/image;s=644x461", "id": "14578547", "color": "brown"},
]

app.get('/api/items', (req, res) => {
  res.send(items)
})

app.listen(port, () => {
  console.log(`Server started on port ${port}`)
})
