import './App.scss';
import Modal from './components/Modal';
import Button from './components/Button';
import React  from "react";

class App extends React.Component {

  state = {
    modal1: false,
    modal2: false,
  }

  close = () => {
    console.log('close');
    this.setState({
      modal1: false,
      modal2: false,
    })
  }

  openFirstModal = () =>{
    this.setState({
      ...this.state,
      modal1: true,
    })
  }

  openSecondModal = () =>{
    this.setState({
      ...this.state,
      modal2: true,
    })
  }
  render() {

    return (
      <div className="App">
      <header className="App-header">
       
          {!this.state.modal1&& <div><Button 
        text ={'Open first modal'}
        background = {'#8071a8'}
        onClick={this.openFirstModal}
        /><Button 
        text ={'Open second modal'}
        background = {'#ff6f61'}
       onClick={this.openSecondModal}
       /> </div>}
        {/* <Button 
        text ={'Open first modal'}
        background = {'#8071a8'}
        onClick={this.openFirstModal}
        />
        <Button 
         text ={'Open second modal'}
         background = {'#ff6f61'}
        onClick={this.openSecondModal}
        /> */}

{this.state.modal1&&<Modal
        close={this.close}
        closeButton={false}
        header={'Why does a round pizzacome in square box?'}
        text={'we will never know.'}
        // isOpened={this.state.modal1}
        onModalClose={() => this.setState({ ...this.state, modal1: false })}
        action = {
          [<Button 
          text ={'well done'}
          background = {'#b55a30'}
          />
         ,
         <Button 
          text ={'OK'}
          background = {'#f5df4d'}
         />]
        }
      />}
     
    {this.state.modal2&&<Modal
        close={this.close}
        closeButton={true}
        header={'Why do people push the buttons of a remote harder when they know the batteries are dead?'}
        // isOpened={this.state.modal2}
        onModalClose={() => this.setState({ ...this.state, modal2: false })}
        action = { 
          [<Button 
          text ={'LOL'}
          background = {'#56c6a9'}
         />
        ]}
      />}
     
       
      </header>

   
    </div>
    )
  };
}

export default App;
