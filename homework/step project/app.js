const TAGS = {
   tabServiceContent: document.querySelector('.service-tabServiceContent'),
   tabsServiceContent: document.querySelectorAll('.service-tabServiceContent-title'),
   tabsServiceContentText: document.querySelectorAll('.tabServiceContent-item'),
   tabWorkContent: document.querySelector('.tabs-work'),
   tabsWorkContent: document.querySelectorAll('.tab-work-title'),
   newsItems: document.querySelector('.news-items'),
   btnPreview: document.querySelector('.btn-preview'),
   btnNext: document.querySelector('.btn-next'),
   carouselItems: document.querySelector('.carousel-items')


}


// tabs for servise
const setDataFortabServiceContent = (item) => {
   item.forEach((element) => {
      let id = element.innerText
      element.setAttribute('data-tab', `${id}`)
   })

}
const addEventTotabServiceContent = () => {
   TAGS.tabServiceContent.addEventListener('click', function (e) {
      setDataFortabServiceContent(TAGS.tabsServiceContent)
      let tabId = e.target.getAttribute('data-tab')
      let currentTab = document.getElementById(`${tabId}`)
      TAGS.tabsServiceContentText.forEach(function (e) {
         e.classList.remove('tabServiceContent-item-active')
      })
      currentTab.classList.add('tabServiceContent-item-active')

   })
   TAGS.tabServiceContent.addEventListener('click', function (e) {
      let li = e.target
      for (let tabs of TAGS.tabServiceContent.children) {
         tabs.classList.remove('service-tab-active')
      }
      li.classList.add('service-tab-active')
   })
}
addEventTotabServiceContent()




// tabs for work
const backSideOfImages = () => {
   let images = document.querySelectorAll('.work-item')
   for (let img of images) {
      let projectName = img.querySelector('.back-name-project')
      let sect = img.getAttribute('class')
      let res = sect.replace('work-item img-work all', '')
      res = res.replace('img-work-more', '').trim()
      res = res[0].toUpperCase() + res.slice(1).replace('-', ' ')
      let nameSect = img.querySelector('.back-name-section')
      nameSect.innerText = res
      let id = img.getAttribute('id')
      projectName.innerText = id
   }
}
backSideOfImages()
const activeTabWork = () => {
   document.querySelector('[data-tab="all"]').classList.add('tab-work-title-active')
   document.querySelector(".btn-show-more").style.display = 'block'
}
const addEventTotabWorkContent = () => {
   TAGS.tabWorkContent.addEventListener('click', filteredTabContent)
   TAGS.tabWorkContent.addEventListener('click', function (e) {
      let tabId = e.target
      if (tabId === document.querySelector('.tabs-work')) {
      } else {
         for (let tab of TAGS.tabWorkContent.children) {
            tab.classList.remove('tab-work-title-active')
         }
         tabId.classList.add('tab-work-title-active')
      }

   })
}

const filteredTabContent = (e) => {
   let tabId = e.target.getAttribute('data-tab')
   if (!tabId) { }
   else {
      let filteredImg = document.querySelectorAll(`.${tabId}`)
      hideImages()
      if (filteredImg.length > 12) {
         let arr = Array.from(filteredImg)
         document.querySelector(".btn-show-more").style.display = 'block'
         for (let img of arr.slice(0, 12)) {
            img.style.display = 'block'
         }
         for (let img of arr.slice(12, arr.length)) {
            let btn = document.querySelector(".btn-show-more")
            btn.addEventListener('click', function () {
               img.style.display = 'block'
               btn.style.display = 'none'
            })
         }

      }
      else {
         document.querySelector(".btn-show-more").style.display = 'none'
         for (let img of filteredImg) {
            img.style.display = 'block'
         }
      }
   }
}

const hideImages = () => {
   let allImgs = document.querySelectorAll('.work-item')
   for (let img of allImgs) {
      img.style.display = 'none'
   }
}
const setDataFortabWorkContent = (item) => {
   item.forEach((element) => {
      let id = element.innerText.toLowerCase()
      id = id.split(" ").join("-");
      element.setAttribute('data-tab', `${id}`)
   })

}
const showMoreImg = () => {
   let btn = document.querySelector(".btn-show-more")
   btn.style.display = 'none'

   btn.addEventListener('click', function () {
      let hiddenImg = document.querySelectorAll('.img-work-more')
      for (let img of hiddenImg) {
         img.classList.remove('img-work-more')
         btn.style.display = 'none'
      }
   })
}

addEventTotabWorkContent()
setDataFortabWorkContent(TAGS.tabsWorkContent)
showMoreImg()
activeTabWork()


//carousel
let userArr = [
   {
      userId: "user1",
      userName: 'Ricardo Ochoa',
      userJob: 'Flight attendant',
      userComment: 'With the advent of the electronic computer and related technologies, a number of aids and devices have been developed ostensibly for the purpose of making life easier for blind people.',
   },
   {
      userId: "user2",
      userName: 'Ray Young',
      userJob: 'Web developers',
      userComment: 'Remember that a slate and stylus, a Perkins Braille writer, and a sighted reader, are still the cheapest way to go. Also, do not forget the old reliable cassette recorder.',
   },
   {
      userId: "user3",
      userName: 'IRIS HERRERA',
      userJob: 'Full Stack Developer',
      userComment: 'With the advent of the electronic computer and related technologies, a number of aids and devices have been developed ostensibly for the purpose of making life easier for blind people. Although it is true that technology can make life easier and more pleasant for all of us (witness the pocket calculator).',
   },
   {
      userId: "user4",
      userName: 'Albi Yu',
      userJob: 'UX Designer',
      userComment: 'There are a variety of reactions from partially blind people concerning magnification aids. While one specific magnifier may help one partially blind person, it may prove to be totally useless for another; so you should have some "hands on" experience with any magnification aid before purchasing it.',
   },
   {
      userId: "user5",
      userName: 'Alison Wooten',
      userJob: 'Financial analysts',
      userComment: 'None of the currently available technology is cheap -- at least, not in the area of reading and writing. Remember that a slate and stylus, a Perkins Braille writer, and a sighted reader, are still the cheapest way to go. Also, do not forget the old reliable cassette recorder.',
   },
   {
      userId: "user6",
      userName: 'Deborah Randall',
      userJob: 'Web developers',
      userComment: 'The number one benefit of information technology is that it empowers people to do what they want to do. It lets people be creative. It lets people be productive.',
   },
   {
      userId: "user7",
      userName: 'Alya Bradford',
      userJob: 'Market research analyst',
      userComment: 'Technology is just a tool. In terms of getting the kids working together and motivating them, the teacher is the most important.',
   },
   {
      userId: "user8",
      userName: 'Ishaan Avalos',
      userJob: 'Emergency medical technicia',
      userComment: 'Technology is a gift of God. After the gift of life it is perhaps the greatest of Gods gifts. It is the mother of civilizations, of arts and of sciences.',
   },
   {
      userId: "user9",
      userName: 'Grayson Kaufman',
      userJob: 'UI-UX Designer',
      userComment: '',
   },
   {
      userId: "user10",
      userName: 'Zakaria Driscoll',
      userJob: 'Accounting',
      userComment: 'The number one benefit of information technology is that it empowers people to do what they want to do. It lets people be creative. It lets people be productive. It lets people learn things they did not think they could learn before, and so in a sense it is all about potential.',
   },
]
const changeUserComment = (obj) => {
   let comment = obj.userComment

   if (!comment) {
   } else {
      let activeComment = document.querySelector('.review')
      activeComment.innerText = comment
   }
}
const changeUserJob = (obj) => {
   let job = obj.userJob

   if (!job) { }
   else {
      let activeJob = document.querySelector('.person-proffesion')
      activeJob.innerText = job
   }
}
const changeUserName = (obj) => {
   let name = obj.userName
   if (!name) {

   } else {
      let activeName = document.querySelector('.person-name')
      activeName.innerText = name
   }
}
const selectInfoOfComment = (elem) => {
   let activeUser = elem.getAttribute('id')
   let activeUserObj = {}
   for (let i = 0; i < userArr.length; i++) {
      if (userArr[i].userId == activeUser) {
         activeUserObj = userArr[i]
      }
   }
   changeUserName(activeUserObj)
   changeUserJob(activeUserObj)
   changeUserComment(activeUserObj)
}
const addEventToCarouselItems = () => {
   TAGS.carouselItems.addEventListener('click', function (e) {
      let item = e.target
      for (let img of document.querySelectorAll('.carousel-img')) {
         console.log(img);
         img.classList.remove('active-carousel-item')
      }
      item.classList.add('active-carousel-item')
      showPhoto(item)
      selectInfoOfComment(item)
   })
}
addEventToCarouselItems()
let l = 0
const showPreviewPict = () => {
   let gallery = document.querySelectorAll('.carousel-item')
   l--
   for (let i of gallery) {
      if (l == 0) {
         i.style.left = '0px'
      }
      if (l == 1) {
         i.style.left = '-76px'
      }
      if (l == 2) { i.style.left = '-156px' }
      if (l == 3) { i.style.left = '-236px' }
      if (l == 4) { i.style.left = '-314px' }
      if (l == 5) { i.style.left = '-394px' }
      if (l < 0) { l = 0 }
   }
}
const showNextPict = () => {
   let gallery = document.querySelectorAll('.carousel-item')
   l++
   for (let i of gallery) {
      if (l == 0) {
         i.style.left = '0px'
      }
      if (l == 1) {
         i.style.left = '-76px'
      }
      if (l == 2) { i.style.left = '-154px' }
      if (l == 3) { i.style.left = '-234px' }
      if (l == 4) { i.style.left = '-314px' }
      if (l == 5) { i.style.left = '-394px' }
      if (l == 6) { i.style.left = '-474px' }
      if (l > 6) { l = 6 }
   }
}
const addEventToCarouselBtn = () => {
   TAGS.btnPreview.addEventListener('click', showPreviewPict)
   TAGS.btnNext.addEventListener('click', showNextPict)

}
addEventToCarouselBtn()
const showPhoto = (elem) => {
   let img = elem.getAttribute('src')
   if (!img) {
   } else {
      let icon = document.querySelector('.person-img')
      icon.src = img
   }
}
