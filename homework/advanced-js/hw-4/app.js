const films = 'https://ajax.test-danit.com/api/swapi/films';
const fetchCurrency = (string) => fetch(string).then(response => {
   return response.json()
});

const prepareCurrency = rates => {
   return Promise.all(rates.map(item => fetchCurrency(item)))
}

const renderCharactersCurrency = collection => {
   return collection.map(item => item.name);
}

const renderCurrency = collection => {
   const ul = document.createElement('ul');
   ul.append(...collection.map(rate => {
      const li = document.createElement('li');
      li.innerHTML = `
      <h1>Episod ${rate.episodeId}. ${rate.name} </h1> 
      <p><b>Overview:</b> ${rate.openingCrawl} <p/>
      `
      prepareCurrency(rate.characters)
         .then(res => renderCharactersCurrency(res))
         .then(res => {
            let p = document.createElement('p');
            p.innerHTML = `<b>Characters:</b> ${res.join(',')}`;
            console.log(res);
            li.append(p);
         })
      return li;
   }))
   document.body.append(ul);
}
fetchCurrency(films).then(res => renderCurrency(res))
