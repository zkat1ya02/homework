const menu = document.querySelector('.menu__item')

menu.addEventListener('click', function () {
    const dropDownMenu = document.querySelector('.drop-down-menu')
    dropDownMenu.style.display = (dropDownMenu.style.display == 'block') ? 'none' : 'block'
})
