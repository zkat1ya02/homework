const TAGS = {
   tabContent: document.querySelectorAll('.tabContent-title'),
   contents: document.querySelector('.tabContent-content').querySelectorAll('li'),
}
const hideContent = (content) => {
   content.forEach((element) => {
      element.classList.add('tabContent-item')
   });

}
const addEventTotabContent = () => {
   console.log(document.querySelector('#Akali').classList.add('content-active'))
   TAGS.tabContent.forEach(function (item) {
      if (item.innerText == 'Akali') {
         item.classList.add('active-tabContent')
      }
   })
   TAGS.tabContent.forEach(function (li) {
      li.addEventListener('click', function () {
         let tabId = li.getAttribute("data-tab");
         let currentTab = document.querySelector(`#${tabId}`)

         TAGS.tabContent.forEach(function (li) {
            li.classList.remove('active-tabContent');
         });
         TAGS.contents.forEach(function (li) {
            li.classList.remove('content-active');
         });
         li.classList.add('active-tabContent');
         currentTab.classList.add('content-active')

      });

   });
};
const setDataFortabContent = (item) => {
   item.forEach((element) => {
      let id = element.innerText
      element.setAttribute('data-tab', `${id}`);
   });
}
hideContent(TAGS.contents)
addEventTotabContent(TAGS.contents)
setDataFortabContent(TAGS.tabContent)
