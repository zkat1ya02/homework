const validPrice = function (price) {
   if (+(price) > 0) {
      return true;
   }
   return false;
}
const deleteSpan = function (event) {
   event.target.parentNode.remove()
   const input = document.querySelector('.input-price');
   input.value = '';
}
function correctInput() {
   if (document.querySelector('.incorrect')) {
      document.querySelector('.incorrect').remove()
   }
   let result = document.querySelector('.input-price').value;
   let span = document.createElement('span');
   input = document.querySelector('.input-price')
   input.classList.add('correct-input-price')
   span.innerHTML = `Текущая цена: ${result}`;
   input = document.querySelector('.input-price')
   let div = document.querySelector('.block')
   span.classList.add('price')
   const deleteBtn = document.createElement('img');
   deleteBtn.classList.add('deleteBtn')
   deleteBtn.setAttribute('src', 'btn.png');
   span.appendChild(deleteBtn)

   div.prepend(span)
   deleteBtn.addEventListener('click', deleteSpan)
}
function incorrectInput() {
   if (document.querySelector('.incorrect')) {
   }
   else {
      let span = document.createElement('span');
      input = document.querySelector('.input-price')
      input.classList.add('incorrect-input-price')
      span.innerHTML = 'Please enter correct price.';
      input = document.querySelector('.input-price')
      let div = document.querySelector('.block')
      span.classList.add('incorrect')
      input.after(span)
   }
}
const getValue = function () {
   let result = document.querySelector('.input-price').value;
   if (validPrice(result)) {
      correctInput()
   }
   else {
      incorrectInput()
   }
}
const priceOfInput = function () {
   const input = document.querySelector('.input-price')
   input.addEventListener('change', getValue);
}
priceOfInput()
