const DOM = {
   upperInput: document.querySelector('#upper-input'),
   underInput: document.querySelector('#under-input'),
   upperInputIcon: document.querySelector('#upper-icon-password'),
   underInputIcon: document.querySelector('#under-icon-password'),
   confirmBtn: document.querySelector('.btn'),
}

const addEventListener = () => {
   DOM.underInputIcon.addEventListener('click', showHide)
   DOM.upperInputIcon.addEventListener('click', showHide)
   DOM.confirmBtn.addEventListener('click', checkPassword)
}

const showHide = (e) => {
   const target = e.target;
   const input = target.previousSibling.previousSibling;
   if (input.type === 'password') {
      input.setAttribute('type', 'text')
      target.classList.add('fa-eye-slash')
   }
   else {
      input.setAttribute('type', 'password')
      target.classList.remove('fa-eye-slash')
   }
}
const removePassword = () => {
   DOM.upperInput.value = ''
   DOM.underInput.value = ''
   document.querySelector('.p').remove()
}
const checkPassword = () => {
   let password = DOM.upperInput.value
   console.log(password);
   let confirmPassword = DOM.underInput.value
   console.log(confirmPassword);
   if (password.trim() == confirmPassword.trim()) {
      removePassword()
      setTimeout(function () {
         alert('You are welcome!');
      }, 100);
   }
   else {
      const p = document.createElement('p');
      p.innerText = 'Нужно ввести одинаковые значения(';
      p.classList.add('p')
      DOM.confirmBtn.before(p);


   }
}
addEventListener()
