
const makeArrayFromCollection = () => {
   let images = document.querySelectorAll('.image-to-show')
   let imgArray = []
   for (let img of images) {
      imgArray.push(img.getAttribute('src'))
   }
   return imgArray
}
makeArrayFromCollection()

let index = 0
function change() {
   let images = makeArrayFromCollection()
   document.querySelector("#mainPhoto").src = images[index];
   document.querySelector("#mainPhoto").classList.add('show-img')
   if (index == images.length - 1) {
      index = 0;
   } else {
      index++;
   }
}
let timerId = setInterval(change, 3000)
window.onload = change();

const addEventforButton = () => {
   document.querySelector('.stop').addEventListener('click', stopChanging)
   document.querySelector('.start').addEventListener('click', continueChanging)
}
const continueChanging = () => {
   console.log(document.querySelector('.start'));
   timerId = setInterval(change, 3000)
}
const stopChanging = () => {
   console.log(document.querySelector('.stop'));
   clearTimeout(timerId)
}
addEventforButton()
