$(document).ready(function () {
   $('a[href^="#"]').click(function () {
      $('html, body').animate({
         scrollTop: $('[name="' + $.attr(this, 'href').substr(1) + '"]').offset().top
      }, 500);

      return false;
   });
   $(window).scroll(function () {
      if ($(this).scrollTop() > 600) {
         $('.scroll-to-top').fadeIn();
      } else {
         $('.scroll-to-top').fadeOut();
      }
   });

   $('.scroll-to-top').click(function () {
      $('html, body').animate({ scrollTop: 0 }, 500);
      return false;
   });
   $(".slide-toggle").click(function (e) {
      e.preventDefault();
      $('.block-hot-news').slideToggle(2000);
   });

});
