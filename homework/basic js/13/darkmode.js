let darkMode = localStorage.getItem('darkMode');

const addEventToChangeTheme = () => {
   const darkModeToggle = document.querySelector('.theme');
   darkModeToggle.addEventListener('click', () => {
      darkMode = localStorage.getItem('darkMode');
      if (darkMode !== 'enabled') {
         enableDarkMode()
      } else {
         disableDarkMode()
      }
   })
}
const enableDarkMode = () => {

   document.body.classList.add('darkmode')
   localStorage.setItem('darkMode', 'enabled')
}
const disableDarkMode = () => {
   document.body.classList.remove('darkmode')
   localStorage.setItem('darkMode', null)
}

if (darkMode === 'enabled') {
   enableDarkMode()
}
addEventToChangeTheme()
