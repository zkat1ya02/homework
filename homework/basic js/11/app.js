const DOM = {
   keys: document.querySelectorAll('.btn')
}
const changeClasses = (e) => {
   DOM.keys.forEach(function (item) {
      item.classList.remove('active-btn');
   });
   e.classList.add('active-btn');

}
const addEventListener = () => {
   document.addEventListener('keydown', function (event) {
      DOM.keys.forEach(function (item) {
         if (event.key == item.innerText) {
            changeClasses(item)
         }
      })

   });

}
addEventListener()
