import React, {useState, useEffect} from "react";
import './item.scss';
import PropTypes from 'prop-types' ;
import StarIcon from '../../icons/star.jsx';

const Item = ({item, openModal, closeModal, favoritesArray, setFavorites}) =>{
    const [filledStar, setFilledStar] = useState(false);
    const {title, price, url, setNumber, color} = item;

    useEffect(() => {
        if (favoritesArray.includes(setNumber)){
            setFilledStar(true);
        }
    }, [favoritesArray,])

    const addToFavorite = (setNumber,) =>{
        favoritesArray.push(setNumber);
        setFilledStar(true);
        setItem();
      }
    
      const removeFavorite = (setNumber) =>{
          let favorites = JSON.parse(localStorage.getItem('favorites'));
          let filteredFavorite = favorites.filter((element) =>{
              return element.setNumber !== setNumber
          })
          localStorage.setItem('favorites', JSON.stringify(filteredFavorite));
        setFilledStar(false);
        favoritesArray = [];
        let arrFromObj = filteredFavorite.map(favorite => {return favorite.setNumber});
        favoritesArray.push(...arrFromObj);
        setFavorites(favoritesArray);
      }

      const setItem = () =>{
        let favorites = JSON.parse(localStorage.getItem('favorites'));
        if (!favorites){
            favorites=[];
        } 
        const selectedItem ={
          title: title,
          setNumber: setNumber,
        }
        favorites.push(selectedItem);
        localStorage.setItem('favorites', JSON.stringify(favorites));
        closeModal();
        alert(`You added ${title} to favorites`);
      }
    return(
        <div className="card">
        <img className="productItem__img" src={url}/> 
         <h1 className="productItem__name">
             {title} 
         {filledStar&&
         <div onClick={() => removeFavorite(setNumber, title)}>
             {StarIcon(filledStar)}
         </div> }
         {!filledStar&&
         <div onClick={() => {
            addToFavorite(setNumber, title)
         }
         }>
             {StarIcon(filledStar) }
         </div> }
         
         </h1>
        <p className="price">
            {price} USD
        </p>
        <p>
            {color}
        </p>
       <p>
           <button onClick={() => openModal(setNumber, title)}>Add to Cart</button>
       </p>
        </div>
   
    )
}

Item.propTypes = {
    item : PropTypes.object.isRequired,
    openModal: PropTypes.func.isRequired,
}

export default Item;