import React, {useEffect, useState} from "react";
import Item from "../Item/Item";
import ModalAddToBasket from "../Modal/ModalAddToBasket";
import PropTypes from 'prop-types';
import './Items.scss';

const Items = ({items}) =>{
    const [favoritesArray, setFavorites] = useState([]);
    const [modalToCart, setModalToCart] = useState(false);
    const [selectedItemToCart, setselectedItemToCart] = useState(null);

    useEffect(() => {
        try{
            let favorites = JSON.parse(localStorage.getItem('favorites'));
            if(favorites){favorites.map(favorite =>{
                favoritesArray.push(favorite.setNumber)
            })}
        }catch(e){
            console.log(e);
        }
      
    }, [favoritesArray])

    const openModal = (setNumber, title) =>{
    setModalToCart(true);
    setselectedItemToCart([setNumber, title]);
  }

  const closeModal = () =>{
    setModalToCart(false);
  }


    const productItems = items.map(item => 
    <Item key={item.setNumber} openModal={openModal} closeModal={closeModal} item={item} favoritesArray = {favoritesArray} setFavorites={setFavorites}  />
    )
    return(
        <div>
            {!modalToCart&&<ol className="products__wrapper">{productItems}</ol>}
            {modalToCart&&<><ol className="products__wrapper">{productItems}</ol> <ModalAddToBasket modalToCart={modalToCart} closeModal={closeModal} openModal={openModal} selectedItemToCart={selectedItemToCart} /></>}
        </div>
        
    )
}
Items.propTypes = {
    items : PropTypes.array.isRequired,
}

export default Items;