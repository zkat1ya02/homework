import React, {useEffect} from "react"
import PropTypes from 'prop-types' 
import './Modal.scss'

const ModalAddToBasket = ({modal, closeModal, openModal, selectedItemToCart }) =>{

 

    const setItem = () =>{
      let cart = JSON.parse(localStorage.getItem('cart'));
      if (!cart){
        cart=[]
      } 
      const selectedItem ={
        title: selectedItemToCart[1],
        setNumber: selectedItemToCart[0],
      }
      cart.push(selectedItem)
      localStorage.setItem('cart', JSON.stringify(cart))
      closeModal()
      alert(`You added ${selectedItemToCart[1]} to shopping cart`)
    }

 return(
        <div 
      className='modal__wrapper '
      onClick={closeModal}
    >
      <div
        className='modal__body'
        onClick={e => e.stopPropagation()}
      >
        <div className='modal__close' 
        onClick={closeModal}
        > x </div>
        <h2>{`Do you want to add ${selectedItemToCart[1]} to shopping cart?`}</h2>
        <hr />
       <button className="btn" onClick={setItem}>
           yes
       </button>
       <button className="btn" onClick={closeModal}>
         no
       </button>

      </div>
    </div>

    )
}

ModalAddToBasket.propTypes = {
  openModal: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired,
  selectedItemToCart: PropTypes.array.isRequired,
}

export default ModalAddToBasket;