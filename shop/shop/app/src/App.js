import React, {useEffect, useState} from 'react';
import axios from 'axios';
import Items from "./components/Items/Items";

const App = () => {
  const [items, setItems] = useState([]);
  
  useEffect(() => {
    axios('/api/items')
      .then(res => {
        setItems(res.data);
      })
  }, []);
  return (
    <div className="App">
      <Items items={items} />
    </div>
  )
}

export default App;
