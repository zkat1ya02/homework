import reducer from './reducers';

export {default as favoritesOperations} from './operatios';
export {default as favoritesSelectors} from './selectors';

export default reducer;