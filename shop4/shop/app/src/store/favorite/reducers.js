import types from './types';

const initialState = {
    data: JSON.parse(localStorage.getItem('favorites')) || [],
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case types.TOGGLE_FAVORITES: {
            const itemId = action.data;
            let newFavorites;
            if (state.data.includes(itemId)) {
                newFavorites = state.data.filter(id =>{
                  return   id !== itemId;
                } )
            } else {
                newFavorites = [...state.data, itemId];
            }
            localStorage.setItem('favorites', JSON.stringify(newFavorites));
            return {...state, data: newFavorites};
        }
        default:
            return state
    }
}
export default reducer;

