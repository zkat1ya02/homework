const getFavorites = () => state => state.favorites.data;

export default {
    getFavorites
}