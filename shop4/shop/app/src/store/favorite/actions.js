import types from './types';

const toggleFavorite  = (item) => {
    return {
        type: types.TOGGLE_FAVORITES,
        data: item
    };
};

export default {
    toggleFavorite,
}