import axios from "axios";
import actions from "./actions";

const getItems = () => dispatch => {
    axios('/api/items')
        .then(res => {
          dispatch(actions.setItemsData(res.data));
        })
}

export default {
    getItems,
}
