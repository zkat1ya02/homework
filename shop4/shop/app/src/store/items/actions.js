import types from './types';

const setItemsData = (itemsData) => ({ type: types.SET_MOVIES_DATA, data: itemsData})

export default {
    setItemsData,
}