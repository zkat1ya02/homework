import {combineReducers} from "redux";
import items from './items';
import cart from './cart';
import favorites from './favorite';

const reducer = combineReducers({
    items,
    cart,
    favorites
})

export default reducer;