import types from './types';

const setItemsData = (itemsData) => ({ type: types.SET_MOVIES_DATA, data: itemsData});
const addItemToCart  = (item) => {
    return {
        type: types.ADD_TO_CART,
        data: item
    };
};

const removeFromCart  = (item) => {
    return {
        type: types.REMOVE_FROM_CART,
        data: item
    };
};

const buyItems = () =>{
    return{
        type: types.BUY_ITEMS,
        data: []
    }
}

export default {
    setItemsData,
    addItemToCart,
    removeFromCart,
    buyItems,
}