import types from './types';

const initialState = {
    data: JSON.parse(localStorage.getItem('cart')) || [],
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case types.ADD_TO_CART: {
            if(state.data){
                const id = state.data.map((item) =>{
                    return item.id
                })
                if(id.includes(action.data.id)){
                    action.data.inCart +=1
                    localStorage.setItem('cart', JSON.stringify( [...state.data] ));
                    return {...state, data:[...state.data] };
                }
                else{
                    action.data.inCart = 1
                    localStorage.setItem('cart', JSON.stringify( [...state.data, action.data]));
                    return {...state, data: [...state.data, action.data]};
                }
            }
        }
        case types.REMOVE_FROM_CART: {
           let newCart = state.data.filter(item =>{return (item.id !== action.data.id)})
            localStorage.setItem('cart', JSON.stringify(newCart));
            return {...state, data: newCart};
        }
        case types.BUY_ITEMS: {
            console.log('dfdfgdfg');
            localStorage.setItem('cart', JSON.stringify([]));
            return {...state, data: []}
        }
        default:
            return state
    }
}
export default reducer;

