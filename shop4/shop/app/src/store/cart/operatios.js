import axios from "axios";
import actions from "./actions";

const getItems = () => dispatch => {
    axios('/api/items')
        .then(res => {
            dispatch(actions.setItemsData(res.data));
        })
}

const addToCart = (item) => dispatch => {
            dispatch(actions.addItemToCart(item));
}

const removeFromCart = (item) => dispatch => {
    dispatch(actions.removeFromCart(item));
}

const buyItems = () => dispatch =>{
    dispatch(actions.buyItems())
}

export default {
    getItems,
    addToCart,
    removeFromCart,
    buyItems
}

