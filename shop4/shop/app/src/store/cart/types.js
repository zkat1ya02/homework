const SET_MOVIES_DATA = 'shop/cart/SET_MOVIES_DATA';
const ADD_TO_CART = 'shop/cart/ADD_TO_CART';
const REMOVE_FROM_CART = 'shop/cart/REMOVE_FROM_CART';
const BUY_ITEMS = 'shop/cart/BUY_ITEMS'

export default {
    SET_MOVIES_DATA,
    ADD_TO_CART,
    REMOVE_FROM_CART,
    BUY_ITEMS
}