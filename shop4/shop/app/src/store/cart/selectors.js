const getMovies = () => state => state.items.data;

const getCart = () => state => state.cart.data;

export default {
    getMovies,
    getCart
}