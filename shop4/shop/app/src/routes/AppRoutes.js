import React from "react";
import { Route } from "react-router";
import { Switch, Redirect} from "react-router-dom";
import Items from "../components/Items/Items";
import Cart from "../pages/Cart/Cart";
import Favorites from "../pages/Favorites/Favorites";
import Formik from "../components/Formik/Formik";
import Page404 from "../pages/Page404/Page404";

const AppRoutes = ({favoritesArray ,setFavorites, modalToCart, selectedItemToCart,openModal, closeModal, setItemToCart, btnAddtoCart, btnRemoveItemFromCart, addToCartModal, removeItemFromCartModal, removeItemFromCart}) =>{


    return(
        <Switch>
        <Redirect exact from = '/' to = '/items'/>
          <Route exact path = "/items">
        <Items
        removeItemFromCart = {removeItemFromCart}
        addToCartModal = {addToCartModal}
        favoritesArray = {favoritesArray}
        setFavorites = {setFavorites}
        openModal = {openModal}
        btnAddtoCart = {btnAddtoCart}
        closeModal = {closeModal}
        modalToCart= {modalToCart} 
        setItemToCart = {setItemToCart}
        selectedItemToCart = {selectedItemToCart} 
        />
          </Route>
          <Route exact path = "/cart">
          <Cart
            removeItemFromCart = {removeItemFromCart}
            removeItemFromCartModal = {removeItemFromCartModal}
            favoritesArray = {favoritesArray}
            setFavorites = {setFavorites}
            openModal = {openModal}
            closeModal = {closeModal}
            modalToCart = {modalToCart}
            setItemToCart = {setItemToCart}
            selectedItemToCart = {selectedItemToCart} 
            btnRemoveItemFromCart = {btnRemoveItemFromCart}
          />
          </Route>
          <Route  exact path = "/favorites">
          <Favorites
             modalToCart = {modalToCart}
             addToCartModal = {addToCartModal}
             openModal = {openModal}
             closeModal = {closeModal}
             btnAddtoCart = {btnAddtoCart}
             setItemToCart = {setItemToCart}
             selectedItemToCart = {selectedItemToCart} 
          />
          </Route>
          {/* <Route path='/buy'>
            <Formik
             removeItemFromCart = {removeItemFromCart}
             removeItemFromCartModal = {removeItemFromCartModal}
             favoritesArray = {favoritesArray}
             setFavorites = {setFavorites}
             openModal = {openModal}
             closeModal = {closeModal}
             modalToCart = {modalToCart}
             setItemToCart = {setItemToCart}
             selectedItemToCart = {selectedItemToCart} 
             btnRemoveItemFromCart = {btnRemoveItemFromCart}
            />
          </Route> */}
          <Route path='*'>
            <Page404/>
          </Route>
          </Switch>
    )
};

export default AppRoutes;