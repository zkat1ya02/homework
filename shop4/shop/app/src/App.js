import React, {useState} from 'react';
import SideBar from './components/SideBar/SideBar';
import AppRoutes from './routes/AppRoutes';
import {store} from '../src/index';


const App = () => {
  const [favoritesArray, setFavorites] = useState(JSON.parse(localStorage.getItem('favorites')) || []);
  const [addToCartModal] = useState(true);
  const [removeItemFromCartModal] = useState(true);
  const [modalToCart, setModalToCart] = useState(false);
  const [btnAddtoCart, ] = useState(true);
  const [btnRemoveItemFromCart,] = useState(true);
  const [selectedItemToCart, setselectedItemToCart] = useState(null);

  const openModal = (item) =>{
    setModalToCart(true);
    setselectedItemToCart(item);
  }

  const closeModal = () =>{
    setModalToCart(false);
  }

  const setItemToCart = () =>{
    closeModal();
    alert(`You added ${selectedItemToCart.title} to shopping cart`);
  }

  const removeItemFromCart = () =>{
      closeModal();
      alert(`You removed ${selectedItemToCart.title} from the shopping cart`);
  }

  return (
    <div className = "App">
      <SideBar/>
      <AppRoutes
       addToCartModal = {addToCartModal}
       removeItemFromCart = {removeItemFromCart}
       closeModal = {closeModal}
       removeItemFromCartModal = {removeItemFromCartModal}
       favoritesArray = {favoritesArray}
       setFavorites = { setFavorites} 
       openModal = {openModal}
       modalToCart = {modalToCart} 
       btnAddtoCart = {btnAddtoCart}
       btnRemoveItemFromCart = { btnRemoveItemFromCart}
       setModalToCart = {setModalToCart} 
       setItemToCart = {setItemToCart}
       selectedItemToCart = {selectedItemToCart} 
       setselectedItemToCart = {setselectedItemToCart}
      />
    </div>
  )
}
export default App;
