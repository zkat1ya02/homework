import React, {useEffect} from "react";
import {connect} from "react-redux";
import Item from "../Item/Item";
import Modal from "../Modal/Modal";
import './Items.scss';
import {itemsOperations} from "../../store/items";
import {useSelector} from "react-redux";
import {cartOperations, cartSelectors} from "../../store/cart";

const Items = ({items, getItems, favoritesArray, setFavorites, modalToCart, selectedItemToCart, openModal, closeModal, setItemToCart, btnAddtoCart,  btnRemoveItemFromCart, addToCartModal, removeItemFromCart}) => {
    const cartArray = useSelector(cartSelectors.getCart());
        useEffect(() => {
            if (items.length === 0){
                getItems()
            }
            }, [getItems]);

    const productItems = items.map(item => 
    {
        if (cartArray.map((itemCart) =>{ return itemCart.id}).includes(item.id)){
            item.inCart= (cartArray.find(itemCart => itemCart.id == item.id )).inCart
        }
    return <Item 
    key = {item.id}
    openModal = {openModal} 
    closeModal = {closeModal} 
    item = {item} 
    btnAddtoCart = {btnAddtoCart}
    favoritesArray = {favoritesArray} 
    setFavorites = {setFavorites} 
    btnRemoveItemFromCart = {btnRemoveItemFromCart}
    />}
    )
    return(
        <>
            <ol className="products__wrapper">{productItems}</ol>
            {modalToCart&&
            <Modal
             modalToCart={modalToCart} 
             closeModal={closeModal}
             openModal={openModal} 
             selectedItemToCart={selectedItemToCart}   
             setItemToCart = {setItemToCart}
             addToCartModal = {addToCartModal}
             removeItemFromCart = {removeItemFromCart}
              />
              }
        </>
        
    )
}
const mapStateToProps = (state) =>{
    return{
        items: state.items.data,
    }
}

const mapDispatchToProps = (dispatch) =>{
    return{
        getItems: () => dispatch(itemsOperations.getItems())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Items);

