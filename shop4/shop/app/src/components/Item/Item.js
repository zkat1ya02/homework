import React, {useState, useEffect} from "react";
import './item.scss';
import StarIcon from '../../icons/star.jsx';
import {connect} from "react-redux";
import {favoritesOperations} from "../../store/favorite";

const Item = ({item, openModal, btnAddtoCart, btnRemoveItemFromCart, toggleFavorite}) => {
    const [filledStar, setFilledStar] = useState(false);
    const {title, price, url, id, color} = item;

    useEffect(() => {
        try {
            // console.log(cartArray);
            // console.log(item.inCart);
            // const idCart = cartArray.map((item) =>{
            //     return item.id
            // })
            // console.log(idCart);
            // if(idCart.includes(id)){
            //     // action.data.inCart +=1
            // }
            let favoritesArray = []
            favoritesArray.push(...JSON.parse(localStorage.getItem('favorites')))
            favoritesArray.forEach((item) => {
                if (item.includes(id)) {
                    setFilledStar(true);
                }
            })
        } catch (e) {
            console.log(e);
        }
    }, [id])

    const handleFavoriteClick = () => {
        toggleFavorite(item.id)
        filledStar ?  setFilledStar(false) :  setFilledStar(true)
    }
        return (
            <div className="card">
                <img className="productItem__img" alt="product" src={url}/>
                <h1 className="productItem__name">
                    {title}
                    <div onClick={() => {handleFavoriteClick()}}> {StarIcon(filledStar)} </div>
                </h1>
                <p className="price">{price} USD</p>
                <p>{color} </p>
                {btnAddtoCart && <p>
                    <button onClick={() => openModal(item)}>Add to Cart</button>
                </p>}
                {item.inCart && <p>{item.inCart}</p>}
                {btnRemoveItemFromCart && <p>
                    <button onClick={() => openModal(item)}>Remove from Cart</button>
                </p>}  
            </div>

        )
    }


const mapStateToProps = (state) =>{
    return{
        cart: state.cart.data
    }
}
const mapDispatchToProps = (dispatch) =>{
    return{
        toggleFavorite: (id) => dispatch(favoritesOperations.toggleFavorite(id))
    }

}
export default connect(mapStateToProps, mapDispatchToProps )(Item);