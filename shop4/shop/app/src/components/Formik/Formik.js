import * as yup from 'yup';
import React from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import {connect} from "react-redux";
import { cartOperations } from '../../store/cart';
import './Formik.scss';


const validationShema = yup.object().shape({
  name: yup
  .string()
  .required("Name is a required field")
  .min(1, "Name must be at least 1 characters"),
  secondName:  yup
  .string()
  .required("Second name is a required field")
  .min(3, "Name must be at least 3 characters"),
  age: yup
  .number()
  .typeError('you must specify a number')
  .required("Please supply your age")
  .min(14, "You must be at least 14 years")
  .max(120, "You must be at most 120 years"),
  address: yup
  .string()
  .required("Address is a required field"),
  phoneNumber: yup
  .string()
  .required()
  .matches(/^[0-9]+$/, "Must be only digits")
  .min(5, 'Must be exactly 5 digits')
  .max(5, 'Must be exactly 5 digits')
})

 
 const Checkout = ({handleBlur, setModalBuy, cartArray, buyItems}) => {

  return(
  <div className='form__wrapper' onClick={() => {setModalBuy(false)}}>
   <div className='form'  onClick = {e => e.stopPropagation()}>
     <Formik
       initialValues={{
            name: '',
            secondName: '',
            age: '',
            address: '',
            phoneNumber: '',
          }}
          validationSchema={validationShema}
         onSubmit={(values, { setSubmitting }) => {
           console.log(JSON.stringify(values, null, 2));
           console.log(cartArray);
           setModalBuy(false);
           setSubmitting(false);
           buyItems()
       }}
     >
       {({ isSubmitting }) => (
         <Form>
           <Field type="text" name="name" placeholder="name" className="form__control" />
           <ErrorMessage name="name" component="div" />
           <Field type="text" name="secondName"  placeholder="second name"  className="form__control" />
           <ErrorMessage name="secondName" component="div" />
           <Field type="text" name="age"  placeholder="age"  className="form__control" />
           <ErrorMessage name="age" component="div" />
           <Field type="text" name="address"  placeholder="address"  className="form__control" />
           <ErrorMessage name="address" component="div" />
           <Field type="text" name="phoneNumber"  placeholder="phone number"  className="form__control"/>
           <ErrorMessage name="phoneNumber" component="div" />
           <button type='submit' disabled={isSubmitting} onClick={handleBlur}  className="btn btn-primary" >Checkout</button>
           
         </Form>
         
       )}
     </Formik>
   </div>
   </div>
 )};

 const mapDispatchToProps = (dispatch) =>{
  return{
      buyItems: () => dispatch(cartOperations.buyItems())
  }
}
 
 export default connect(null, mapDispatchToProps)(Checkout);




