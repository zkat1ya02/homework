import React from "react"
import './Modal.scss'
import {cartOperations} from "../../store/cart";
import {connect} from "react-redux";

const Modal = ({closeModal,addToCart, removeFromCart, selectedItemToCart, setItemToCart, addToCartModal, removeItemFromCartModal, removeItemFromCart}) =>(

  <div  className = 'modal__wrapper ' onClick = {closeModal}>
      <div className = 'modal__body' onClick = {e => e.stopPropagation()} >
        <div className = 'modal__close' onClick = {closeModal}> x </div>
        {addToCartModal&&
        <>
        <h2>{`Do you want to add ${selectedItemToCart.title} to shopping cart?`}</h2>
        <hr />
       <button className = "btn" onClick = {() => {addToCart(selectedItemToCart); setItemToCart()}}> yes </button>
       <button className = "btn" onClick = {closeModal}> no </button>
       </>
       }
       {removeItemFromCartModal&&
       <>
       <h2>{`Do you want to remove ${selectedItemToCart.title} from the shopping cart?`}</h2>
        <button className = "btn" onClick = {() => {removeFromCart(selectedItemToCart); removeItemFromCart(); }}> yes </button>
       <button className = "btn" onClick = {closeModal}> no </button>
       </>
       }
      </div>
  </div>
)
const mapDispatchToProps = (dispatch) =>{
    return{
        addToCart: (item) => dispatch(cartOperations.addToCart(item)),
        removeFromCart:  (item) => dispatch(cartOperations.removeFromCart(item))
    }
}
const mapStateToProps = (state) =>{
    return{
        cart: state.cart.data
    }
}

export default  connect( mapStateToProps, mapDispatchToProps)(Modal);