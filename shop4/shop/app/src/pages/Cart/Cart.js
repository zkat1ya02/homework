import React from "react"
import {useState, useEffect } from "react";
import axios from 'axios';
import Item from "../../components/Item/Item";
import Formik from "../../components/Formik/Formik"
import Modal from "../../components/Modal/Modal";
import {useSelector} from "react-redux";
import {cartSelectors} from "../../store/cart";
import './Cart.scss';

const Cart = ({selectedItemToCart, setCart, favoritesArray, setFavorites,  openModal, closeModal, modalToCart, setItemToCart, btnRemoveItemFromCart,  removeItemFromCartModal, removeItemFromCart, btnBuy }) =>{
    // const [unique, setUniqueItems] = useState([]);
    const items = useSelector(cartSelectors.getMovies());
    const cartArray = useSelector(cartSelectors.getCart());
    const [modalBuy, setModalBuy] = useState(false)
    useEffect(() => {
        try{
            if (items.length === 0){
                axios('/api/items')
                    .then(res => {
                        items.push(...res.data);
                    })
            } else{
            }
        }catch(e){
            console.log(e);
        }

    }, [cartArray, items])

    const productItems = () =>{
        const productItems = cartArray.map(item =>
        <Item 
        key={item.id}
        favoritesArray ={favoritesArray}
        setFavorites = {setFavorites}
        setCart = {setCart}
        openModal={openModal} 
        closeModal={closeModal} 
        item={item}   
        btnRemoveItemFromCart = {btnRemoveItemFromCart}
        btnBuy = {btnBuy}
        />)
         return productItems
    } 
    
 return(
    <>
     {cartArray == 0 &&<ol className = "products__wrapper">The cart is empty</ol>}
     {cartArray.length >0 &&
     <>
     <ol className = "products__wrapper">{productItems()}</ol>
     <button onClick={() => {setModalBuy(true)}} className="btn btn__buy">Buy</button>
    </>
     }
     {modalToCart&&
     <Modal
     modalToCart={modalToCart} 
     closeModal={closeModal} 
     openModal={openModal} 
     selectedItemToCart={selectedItemToCart} 
     setItemToCart = {setItemToCart}
     removeItemFromCartModal = {removeItemFromCartModal}
     removeItemFromCart = {removeItemFromCart}
     />
     } 
    {modalBuy&&
    <Formik 
    setModalBuy = {setModalBuy}
    cartArray = {cartArray}
    />}
    </>
    )
}

export default Cart;

