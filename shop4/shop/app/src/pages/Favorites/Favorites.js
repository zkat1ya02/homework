import React, {useEffect, useState} from "react"
import Item from "../../components/Item/Item";
import Modal from "../../components/Modal/Modal";
import {itemsOperations} from "../../store/items";
import {favoritesSelectors} from "../../store/favorite";
import {connect, useSelector} from "react-redux";


 const Favorites =  ({items, getItems, favoritesArray, setFavorites, modalToCart, selectedItemToCart, openModal, closeModal, setItemToCart, btnAddtoCart,  btnRemoveItemFromCart, addToCartModal, removeItemFromCart}) => {
     const selectedArray = useSelector(favoritesSelectors.getFavorites())

     useEffect(() => {
         if (items.length === 0){
             getItems()
         }
     }, [getItems]);

     const productItems = () => {
         const selectedItemsObj = selectedArray.map((selected) =>{
             return  items.find((item)=>{
                 return item.id === selected
             })
         })
         if (selectedItemsObj[0] === undefined){
             return
         }
         return  selectedItemsObj.map(item =>
             <Item
                 key={item.id}
                 openModal={openModal}
                 closeModal={closeModal}
                 item={item}
                 btnAddtoCart={btnAddtoCart}
                 favoritesArray={favoritesArray}
                 setFavorites={setFavorites}
                 btnRemoveItemFromCart={btnRemoveItemFromCart}
             />
         )
     }
     return(
         <>
             {selectedArray.length === 0 && <ol className="products__wrapper">There is no prodact in selected</ol>}
             {selectedArray && <ol className="products__wrapper">{productItems()}</ol>}
             {modalToCart&&
             <Modal
                 modalToCart={modalToCart}
                 closeModal={closeModal}
                 openModal={openModal}
                 selectedItemToCart={selectedItemToCart}
                 setItemToCart = {setItemToCart}
                 addToCartModal = {addToCartModal}
                 removeItemFromCart = {removeItemFromCart}
             />
             }
         </>

     )
}
const mapStateToProps = (state) =>{
    return{
        items: state.items.data,
    }
}

const mapDispatchToProps = (dispatch) =>{
    return{
        getItems: () => dispatch(itemsOperations.getItems())
    }
}

export default connect(mapStateToProps,
    mapDispatchToProps
)(Favorites);

